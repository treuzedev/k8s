# k8s

Kelsey Hightower's [K8s The Hard Way](https://github.com/kelseyhightower/kubernetes-the-hard-way) in AWS, Terraform and Bash.

<br>

## Requirements

* cfssl and cfssljson
* AWS CLI V2
* Terraform
* Kubectl

<br>

## Workflow

1) VPC, subnets, RTs, EIPs, IGW, NGW creation
2) SGs creation (K8s nodes, ALB, user-bastion, bastion-nodes, all-out)
3) SSH public key upload (access to all nodes)
4) PKI provision (CA Authority and Admin Certificates): IAM Role, S3 Buckets, Lambdas, null-resources (Lambda invocations) 
5) 3 Control Plane nodes
6) ASG + LT for Worker nodes
7) LB with static EIP
8) Configure cluster initial resources (DNS, etc..)
9) Smoke test (TODO)
10) Pipeline deployment (TODO)

<br>

## PKI

Provisioned via Lambda function, using [this](https://gitlab.com/treuzedev/k8s-helpers/docker-images/pki-files) base image to do so when called.

Everything is stored in a S3 bucket.

<br>

## Kubeconfigs

Also provisioned via a Lambda function, and also stores everything in a S3 bucket. Image [here](https://gitlab.com/treuzedev/k8s-helpers/docker-images/kubeconfig-files).

<br>

### K8s components

* Certificates from a CA (cfssl)
* Admin user
* kubectl
* Controller Manager
* Kube Proxy
* Scheduler
* API Server
* kubelet
* etcd
* CNI networking
* Container runtime (cri + containerd + runc)
* Pods network rules
* DNS (user [kube-router](https://github.com/cloudnativelabs/kube-router))

All components are initialized via user-data files on every boot.

Every node in the cluster lives in a private subnet. Access to the internet is done via NGW. Access to the nodes is done via LB (public access to services; access via kubectl) and a bastion instance.

<br>

##### For the future

* Use Ansible to configure the instances
