#!/usr/bin/env python3

# OS ENVS
# BUCKET_NAME
# PREFIX
# NUMBER_OF_RETRIES
# EXPECTED_NUMBER_OF_FILES
# CONFIG_FILE_PATH

import os
import sys
import time
import json
import boto3
    
def get_s3_client():
  return boto3.client('s3')
  
def get_kubeconfig(s3):
  flag = True
  count = 0
  number_of_retries = int(os.environ.get('NUMBER_OF_RETRIES'))
  expected_number_of_files = int(os.environ.get('EXPECTED_NUMBER_OF_FILES'))
  bucket = os.environ.get('BUCKET_NAME')
  prefix = os.environ.get('PREFIX')
  config_file_path = os.environ.get('CONFIG_FILE_PATH')
  try:
    while flag == True:
      response = s3.list_objects_v2(
        Bucket=bucket,
        Prefix=prefix,
      )
      number_of_items = int(response['KeyCount'])
      if number_of_items == expected_number_of_files:
        print('Number of files in the folder matches the expected number of files.')
        flag = False
      else:
        print('Waiting for files to be available..')
        if count > number_of_retries:
          print('Number of retries exceeded. Exiting.')
          sys.exit(1)
        count += 1
        time.sleep(60)
  except:
    print("Unexpected error:", sys.exc_info()[0])
    sys.exit(2)
  try:
    home_dir = os.path.expanduser('~')
    response = s3.get_object(
      Bucket=bucket,
      Key=config_file_path
    )
    bynary_string = response['Body'].read()
    with open(f"{home_dir}/.kube/config", "wb") as file:
      file.write(bynary_string)
    with open(f"{home_dir}/.kube/config", "r+") as file:
      print(file.read())
  except:
    print("Unexpected error:", sys.exc_info()[0])
    sys.exit(3)
  return True

def main():
  s3 = get_s3_client()
  get_kubeconfig(s3)
  return True

if __name__ == "__main__":
    main()