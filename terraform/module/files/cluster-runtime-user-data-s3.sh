#!/bin/bash

#####
# PREPARATIONS
#####

# variables that dont need awscli
export SLACK_WEBHOOK_URL="${slack_webhook_url}"
export BUCKET="${bucket}"
export PKI_FILES_DIR="${pki_files_dir}"
export KUBECONFIG_FILES_DIR="${kubeconfig_files_dir}"
export WORKERS_FILES_DIR="${workers_files_dir}"
export NUMBER_OF_PKI_FILES="${number_of_pki_files}"
export NUMBER_OF_KUBECONFIG_FILES="${number_of_kubeconfig_files}"
export ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)
export C="${c}"
export L="${l}"
export LB_PUBLIC_IP="${lb_public_ip}"
export WORKER_TYPE="${worker_type}"
export CLUSTER_CIDR="${cluster_cidr}"
export CLUSTER_NAME="${cluster_name}"

# TODO: what if there is more than one nat? why is an external ip needed? can we use the lb's ip?
# TODO: fail if there is no public ip / nat
# variables that need awscli
export NAT_GATEWAY_ID="${nat_gateway_id}"
export EXTERNAL_IP=$(aws ec2 describe-nat-gateways --nat-gateway-ids $NAT_GATEWAY_ID | jq -r '.NatGateways[0].NatGatewayAddresses[0].PublicIp')
export INTERNAL_IP=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)

echo "no step defined" > /tmp/step-bootstrap-$${WORKER_TYPE}


#####
# FUNCTIONS
#####

function curl_slack {
  set -euo pipefail
  STEP=$(cat /tmp/step-bootstrap-$${WORKER_TYPE})
  curl \
    -s \
    -X POST \
    -H "Content-type: application/json" \
    -d "{
      \"text\": \"Error in function '$function' step '$STEP' with exit code '$ERROR'.\nInstance Id: '$ID'.\"
    }"\
    $SLACK_WEBHOOK_URL
  set +euo pipefail
}

function get_node_name {(
  set -euo pipefail
  # TAGS=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$ID" | jq -c '.Tags[]')
  # TAGS_ARRAY=($TAGS)
  # for line in "$${TAGS_ARRAY[@]}"
  # do
  #   KEY=$(echo $line | jq -r '.Key')
  #   if [[ $KEY == "Name" ]]
  #   then
  #     NAME=$(echo $line | jq -r '.Value')
  #   fi
  # done
  # echo $NAME
  cat /etc/hostname
  set +euo pipefail
)}

function tag_instance {(
  set -euo pipefail
  echo "tagging instance.."
  FLAG="false"
  NAME=""
  DATE=$(date +'%s')
  NAME_TAG="$WORKER_TYPE-$DATE"
  TAGS=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$ID" | jq -c '.Tags[]')
  TAGS_ARRAY=($TAGS)
  for line in "$${TAGS_ARRAY[@]}"
  do
    KEY=$(echo $line | jq -r '.Key')
    if [[ $KEY == "Name" ]]
    then
      FLAG="true"
      NAME=$(echo $line | jq -r '.Value')
    fi
  done
  if [[ $FLAG == "false" ]]
  then
    aws ec2 create-tags --resources $ID --tags Key=Name,Value=$NAME_TAG
    echo "Instance tagged with 'Name=$NAME_TAG'."
  else
    echo "Instance already had a tag 'Name=$NAME'."
  fi
  set +euo pipefail
)}

function wait_for_files {(
  set -euo pipefail
  echo "waiting for pki files to be in s3 bucket.."
  FLAG="false"
  while [[ "$FLAG" == "false" ]]
  do
    OBJECTS=$(aws s3 ls s3://$BUCKET/$PKI_FILES_DIR --recursive --summarize | grep 'Total Objects:' | awk '{print $NF}')
    if [[ "$OBJECTS" == $NUMBER_OF_PKI_FILES ]]
    then
      echo "files are ready!"
      FLAG="true"
    else
      echo "files not ready yet.."
      sleep 10
    fi
  done
  echo "waiting for kubeconfig files to be in s3 bucket.."
  FLAG="false"
  while [[ "$FLAG" == "false" ]]
  do
    OBJECTS=$(aws s3 ls s3://$BUCKET/$KUBECONFIG_FILES_DIR --recursive --summarize | grep 'Total Objects:' | awk '{print $NF}')
    if [[ "$OBJECTS" == $NUMBER_OF_KUBECONFIG_FILES ]]
    then
      echo "files are ready!"
      FLAG="true"
    else
      echo "files not ready yet.."
      sleep 10
    fi
  done
  set +euo pipefail
)}

function get_pki_files {(
  set -euo pipefail
  echo "- retrieving pki files.."
  mkdir -p /usr/k8s-files/pki
  echo "retrieving ca-config.json"
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/ca-config.json /usr/k8s-files/pki
  echo "retrieving ca.pem and ca-key.pem"
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/ca.pem /usr/k8s-files/pki
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/ca-key.pem /usr/k8s-files/pki
  echo "files retrieved:"
  ls /usr/k8s-files/pki
  set +euo pipefail
)}

function get_kubeconfig_files {(
  set -euo pipefail
  echo "retrieving kubeconfig files.."
  mkdir -p /usr/k8s-files/kubeconfig
  aws s3 cp s3://$BUCKET/$KUBECONFIG_FILES_DIR/kube-proxy.kubeconfig /usr/k8s-files/kubeconfig
  aws s3 cp s3://$BUCKET/$KUBECONFIG_FILES_DIR/remote-admin.kubeconfig /usr/k8s-files/kubeconfig
  aws s3 cp s3://$BUCKET/$KUBECONFIG_FILES_DIR/kube-router.kubeconfig /usr/k8s-files/kubeconfig
  echo "files retrieved:"
  ls /usr/k8s-files/kubeconfig
  set +euo pipefail
)}

function generate_node_certificate {(
  set -euo pipefail
  echo "getting node name" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  NAME=$(get_node_name)
  echo "generating csr for $${WORKER_TYPE}" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  mkdir -p /usr/k8s-files/certs
  cat > /usr/k8s-files/certs/$${NAME}-csr.json << EOF
{
  "CN": "system:node:$${NAME}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "$${C}",
      "L": "$${L}",
      "O": "system:nodes"
    }
  ]
}
EOF
  echo "generating certificate for $${WORKER_TYPE}" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  cfssl gencert \
    -ca=/usr/k8s-files/pki/ca.pem \
    -ca-key=/usr/k8s-files/pki/ca-key.pem \
    -config=/usr/k8s-files/pki/ca-config.json \
    -hostname="$${NAME},$${EXTERNAL_IP},$${INTERNAL_IP}" \
    -profile=kubernetes \
    /usr/k8s-files/certs/$${NAME}-csr.json | cfssljson -bare $${NAME}
  mv $${NAME}.pem $${NAME}-key.pem /usr/k8s-files/certs
  set +euo pipefail
)}

function generate_k8s_config_files {(
  set -euo pipefail
  echo "getting node name" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  NAME=$(get_node_name)
  echo "generating kubelet config file" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  mkdir -p /usr/k8s-files/others
  kubectl config set-cluster $${CLUSTER_NAME} \
    --certificate-authority=/usr/k8s-files/pki/ca.pem \
    --embed-certs=true \
    --server=https://$${LB_PUBLIC_IP}:443 \
    --kubeconfig=$${NAME}.kubeconfig
  kubectl config set-credentials system:node:$${NAME} \
    --client-certificate=/usr/k8s-files/certs/$${NAME}.pem \
    --client-key=/usr/k8s-files/certs/$${NAME}-key.pem \
    --embed-certs=true \
    --kubeconfig=$${NAME}.kubeconfig
  kubectl config set-context default \
    --cluster=$${CLUSTER_NAME} \
    --user=system:node:$${NAME} \
    --kubeconfig=$${NAME}.kubeconfig
  kubectl config use-context default --kubeconfig=$${NAME}.kubeconfig
  mv $${NAME}.kubeconfig /usr/k8s-files/others
  set +euo pipefail
)}

function copy_files_to_s3 {(
  set -euo pipefail
  echo "copying node files to s3" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  aws s3 cp /usr/k8s-files/certs s3://$BUCKET/$WORKERS_FILES_DIR --recursive
  aws s3 cp /usr/k8s-files/others s3://$BUCKET/$WORKERS_FILES_DIR --recursive
  set +euo pipefail
)}

function disable_swap {(
  set -euo pipefail
  echo "disabling swap" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  swapon --show
  swapoff -a
  rm -f /swap.img
  set +euo pipefail
)}

function install_worker_binaries {(
  set -euo pipefail
  echo "prep dirs" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  mkdir -p \
    /etc/cni/net.d \
    /opt/cni/bin \
    /var/lib/kubelet \
    /var/lib/kube-proxy \
    /var/lib/kubernetes \
    /var/run/kubernetes
  if [[ ! -f "/usr/local/bin/crictl" ]]
  then
    echo "installing crictl" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
    wget -q "https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.21.0/crictl-v1.21.0-linux-amd64.tar.gz"
    tar -xvf crictl-v1.21.0-linux-amd64.tar.gz
    chmod +x crictl
    mv crictl /usr/local/bin/
  else
    echo "crictl already installed" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  fi
  if [[ ! -f "/usr/local/bin/runc" ]]
  then
    echo "installing runc" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
    wget -q "https://github.com/opencontainers/runc/releases/download/v1.0.0-rc93/runc.amd64"
    mv runc.amd64 runc
    chmod +x runc
    mv runc /usr/local/bin/
  else
    echo "runc already installed" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  fi
  if [[ ! -f "/opt/cni/bin/flannel" ]]
  then
    echo "installing cni plugins" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
    wget -q "https://github.com/containernetworking/plugins/releases/download/v0.9.1/cni-plugins-linux-amd64-v0.9.1.tgz"
    tar -xvf cni-plugins-linux-amd64-v0.9.1.tgz -C /opt/cni/bin/
  else
    echo "cni plugins already installed" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  fi
  if [[ ! -f "/bin/containerd" ]]
  then
    echo "installing containerd" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
    wget -q "https://github.com/containerd/containerd/releases/download/v1.4.4/containerd-1.4.4-linux-amd64.tar.gz"
    mkdir containerd
    tar -xvf containerd-1.4.4-linux-amd64.tar.gz -C containerd
    mv containerd/bin/* /bin/
  else
    echo "containerd already installed" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  fi
  if [[ ! -f "/usr/local/bin/kubectl" ]]
  then
    echo "installing kubectl" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
    wget -q "https://storage.googleapis.com/kubernetes-release/release/v1.21.0/bin/linux/amd64/kubectl"
    chmod +x kubectl
    mv kubectl /usr/local/bin/
  else
    echo "kubectl already installed" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  fi
  if [[ ! -f "/usr/local/bin/kubelet" ]]
  then
    echo "installing kubelet" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
    wget -q "https://storage.googleapis.com/kubernetes-release/release/v1.21.0/bin/linux/amd64/kubelet"
    chmod +x kubelet
    mv kubelet /usr/local/bin/
  else
    echo "kubectl already installed" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  fi
  if [[ ! -f "/usr/local/bin/kube-proxy" ]]
  then
    echo "installing kube-proxy" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
    wget -q "https://storage.googleapis.com/kubernetes-release/release/v1.21.0/bin/linux/amd64/kube-proxy"
    chmod +x kube-proxy
    mv kube-proxy /usr/local/bin/
  else
    echo "kube-proxy already installed" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  fi
  set +euo pipefail
)}

function configure_cni {(
  set -euo pipefail
  echo "creating bridge network config file" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  POD_CIDR=$(cat /usr/k8s-files/cidrs/pods-cidr)
  cat > /etc/cni/net.d/10-bridge.conf << EOF
{
    "cniVersion": "0.4.0",
    "name": "bridge",
    "type": "bridge",
    "bridge": "cnio0",
    "isGateway": true,
    "ipMasq": true,
    "ipam": {
        "type": "host-local",
        "ranges": [
          [{"subnet": $${POD_CIDR}}]
        ],
        "routes": [{"dst": "0.0.0.0/0"}]
    }
}
EOF
  echo "creating loopback network config file" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  cat > /etc/cni/net.d/99-loopback.conf << EOF
{
    "cniVersion": "0.4.0",
    "name": "lo",
    "type": "loopback"
}
EOF
  set +euo pipefail
)}

function configure_kube_router {(
  set -euo pipefail
  echo "all kube router needs for now is a kubeconfig file; that file is downloaded in get_kubeconfig_files" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  echo "prepping env" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  mkdir -p /var/lib/kube-router
  cp /usr/k8s-files/kubeconfig/kube-router.kubeconfig /var/lib/kube-router/kubeconfig
  set +euo pipefail
)}

function configure_containerd {(
  set -euo pipefail
  echo "creating containerd config file" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  mkdir -p /etc/containerd
  cat > /etc/containerd/config.toml << EOF
[plugins]
  [plugins.cri.containerd]
    snapshotter = "overlayfs"
    [plugins.cri.containerd.default_runtime]
      runtime_type = "io.containerd.runtime.v1.linux"
      runtime_engine = "/usr/local/bin/runc"
      runtime_root = ""
EOF
  echo "creating containerd.service" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  cat > /etc/systemd/system/containerd.service << EOF
[Unit]
Description=containerd container runtime
Documentation=https://containerd.io
After=network.target

[Service]
ExecStartPre=/sbin/modprobe overlay
ExecStart=/bin/containerd
Restart=always
RestartSec=5
Delegate=yes
KillMode=process
OOMScoreAdjust=-999
LimitNOFILE=1048576
LimitNPROC=infinity
LimitCORE=infinity
User=root

[Install]
WantedBy=multi-user.target
EOF
  set +euo pipefail
)}

function configure_kubelet {(
  set -euo pipefail
  echo "getting node name" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  NAME=$(get_node_name)
  echo "prepping env" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  mkdir -p /var/lib/kubernetes /var/lib/kubelet/kubeconfig
  cp /usr/k8s-files/certs/$${NAME}-key.pem /var/lib/kubelet
  cp /usr/k8s-files/certs/$${NAME}.pem /var/lib/kubelet
  cp /usr/k8s-files/others/$${NAME}.kubeconfig /var/lib/kubelet/kubeconfig
  cp /usr/k8s-files/pki/ca.pem /var/lib/kubernetes/
  POD_CIDR=$(cat /usr/k8s-files/cidrs/pods-cidr)
  echo "creating kubelet config file" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  cat > /var/lib/kubelet/kubelet-config.yaml << EOF
---
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
authentication:
  anonymous:
    enabled: false
  webhook:
    enabled: true
  x509:
    clientCAFile: "/var/lib/kubernetes/ca.pem"
authorization:
  mode: Webhook
clusterDomain: "cluster.local"
clusterDNS:
  - "10.32.0.10"
podCIDR: $${POD_CIDR}
resolvConf: "/run/systemd/resolve/resolv.conf"
runtimeRequestTimeout: "15m"
tlsCertFile: "/var/lib/kubelet/$${NAME}.pem"
tlsPrivateKeyFile: "/var/lib/kubelet/$${NAME}-key.pem"
EOF
  echo "creating kubelet.service" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  cat > /etc/systemd/system/kubelet.service << EOF
[Unit]
Description=Kubernetes Kubelet
Documentation=https://github.com/kubernetes/kubernetes
After=containerd.service
Requires=containerd.service

[Service]
ExecStart=/usr/local/bin/kubelet \\
  --config=/var/lib/kubelet/kubelet-config.yaml \\
  --container-runtime=remote \\
  --container-runtime-endpoint=unix:///var/run/containerd/containerd.sock \\
  --image-pull-progress-deadline=2m \\
  --kubeconfig=/var/lib/kubelet/kubeconfig/$${NAME}.kubeconfig \\
  --network-plugin=cni \\
  --register-node=true \\
  --v=2
Restart=on-failure
RestartSec=5
User=root

[Install]
WantedBy=multi-user.target
EOF
  set +euo pipefail
)}

function configure_k8s_proxy {(
  set -euo pipefail
  echo "prepping env" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  mkdir -p /var/lib/kube-proxy/kubeconfig
  cp /usr/k8s-files/kubeconfig/kube-proxy.kubeconfig /var/lib/kube-proxy/kubeconfig
  # echo "getting node name" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  # NAME=$(get_node_name)
  echo "creating kube-proxy config file" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  cat > /var/lib/kube-proxy/kube-proxy-config.yaml << EOF
---
kind: KubeProxyConfiguration
apiVersion: kubeproxy.config.k8s.io/v1alpha1
clientConnection:
  kubeconfig: "/var/lib/kube-proxy/kubeconfig/kube-proxy.kubeconfig"
mode: "iptables"
clusterCIDR: "$${CLUSTER_CIDR}"
EOF
  echo "creating kube-proxy.service" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  cat > /etc/systemd/system/kube-proxy.service << EOF
[Unit]
Description=Kubernetes Kube Proxy
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-proxy \\
  --config=/var/lib/kube-proxy/kube-proxy-config.yaml
Restart=on-failure
RestartSec=5
User=root

[Install]
WantedBy=multi-user.target
EOF
  set +euo pipefail
)}

function start_k8s_worker_services {(
  set -euo pipefail
  echo "reloading daemon" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  systemctl daemon-reload
  echo "starting containerd" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  systemctl enable containerd
  systemctl start containerd
  echo "starting kubelet" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  systemctl enable kubelet
  systemctl start kubelet
  echo "starting kube-proxy" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  systemctl enable kube-proxy
  systemctl start kube-proxy
  set +euo pipefail
)}

function get_pods_cidr {(
  set -euo pipefail
  echo "getting pods cidr" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  echo "prepping env" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  mkdir -p /tmp/cidrs
  FLAG="true"
  while [[ "$FLAG" == "true" ]]
  do
    echo "checking if cidrs.json is unlocked" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
    aws s3 cp s3://$BUCKET/cluster/cidrs.lock /tmp/cidrs/cidrs.lock
    LOCK=$(cat /tmp/cidrs/cidrs.lock)
    if [[ "$LOCK" == "y" ]]
    then
      echo "cidrs.json is still locked" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
      sleep 10
    else
      echo "cidrs.json is unlocked, locking it before continuing" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
      LOCK="y"
      echo $LOCK > /tmp/cidrs/cidrs.lock
      aws s3 cp /tmp/cidrs/cidrs.lock s3://$BUCKET/cluster/cidrs.lock
      FLAG="false"
    fi
  done
  echo "getting available cidrs" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  aws s3 cp s3://$BUCKET/cluster/cidrs.json /tmp/cidrs/cidrs.json
  KEYS=$(cat /tmp/cidrs/cidrs.json | jq 'keys[]')
  KEYS_ARRAY=($KEYS)
  for CIDR in "$${KEYS_ARRAY[@]}"
  do
    FLAG=$(jq -r ".$CIDR" /tmp/cidrs/cidrs.json)
    if [[ "$FLAG" == "true" ]]
    then
      echo "$CIDR is available, updating cidrs.json in s3 to reflect its usage by this node" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
      cat /tmp/cidrs/cidrs.json | jq ".$CIDR = false" > /tmp/cidrs/tmp.json
      aws s3 cp /tmp/cidrs/tmp.json s3://$BUCKET/cluster/cidrs.json
      echo "creating a tmp file holding the cidr to use" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
      mkdir -p /usr/k8s-files/cidrs
      echo $CIDR > /usr/k8s-files/cidrs/pods-cidr
      echo "cidrs.json is locked, unlocking it before continuing" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
      LOCK="n"
      echo $LOCK > /tmp/cidrs/cidrs.lock
      aws s3 cp /tmp/cidrs/cidrs.lock s3://$BUCKET/cluster/cidrs.lock
      echo "cleaning up" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
      rm -rf /tmp/cidrs
      exit 0
    fi
  done
  echo "cidrs.json is locked, unlocking it before continuing" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  LOCK="n"
  echo $LOCK > /tmp/cidrs/cidrs.lock
  aws s3 cp /tmp/cidrs/cidrs.lock s3://$BUCKET/cluster/cidrs.lock
  echo "cleaning up" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  rm -rf /tmp/cidrs
  echo "no cidr available, panicking!" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  exit 1
  set +euo pipefail
)}

function update_cidrs_in_s3 {(
  set -euo pipefail
  echo "prepping env" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  mkdir -p /tmp/cidrs
  FLAG="true"
  while [[ "$FLAG" == "true" ]]
  do
    echo "checking if cidrs.json is unlocked" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
    aws s3 cp s3://$BUCKET/cluster/cidrs.lock /tmp/cidrs/cidrs.lock
    LOCK=$(cat /tmp/cidrs/cidrs.lock)
    if [[ "$LOCK" == "y" ]]
    then
      echo "cidrs.json is still locked"
      sleep 1
    else
      echo "cidrs.json is unlocked, locking it before continuing" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
      LOCK="y"
      echo $LOCK > /tmp/cidrs/cidrs.lock
      aws s3 cp /tmp/cidrs/cidrs.lock s3://$BUCKET/cluster/cidrs.lock
      FLAG="false"
    fi
  done
  echo "getting cidrs.json template" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  aws s3 cp s3://$BUCKET/cluster/cidrs.template.json /tmp/cidrs/tmp-0.json
  USED_CIDRS=$(kubectl get nodes -o jsonpath='{.items[*].spec.podCIDR}' --kubeconfig /usr/k8s-files/kubeconfig/remote-admin.kubeconfig)
  USED_CIDRS_ARRAY=($USED_CIDRS)
  PREV_COUNT=0
  NEXT_COUNT=1
  for CIDR in "$${USED_CIDRS_ARRAY[@]}"
  do
    echo "$CIDR is being used, updating cidrs.json" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
    jq ".\"$CIDR\" = false" "/tmp/cidrs/tmp-$${PREV_COUNT}.json" > "/tmp/cidrs/tmp-$${NEXT_COUNT}.json"
    ((PREV_COUNT+=1))
    ((NEXT_COUNT+=1))
  done
  echo "updating cidrs.json in s3" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  aws s3 cp /tmp/cidrs/tmp-$${PREV_COUNT}.json s3://$BUCKET/cluster/cidrs.json
  echo "cidrs.json is locked, unlocking it before continuing" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  LOCK="n"
  echo $LOCK > /tmp/cidrs/cidrs.lock
  aws s3 cp /tmp/cidrs/cidrs.lock s3://$BUCKET/cluster/cidrs.lock
  FLAG="false"
  echo "cleaning up" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  rm -rf /tmp/cidrs
  set +euo pipefail
)}

function set_hostname {(
  set -euo pipefail
  echo "changing machine hostname to match with name tag" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  echo "getting node name" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  NAME=$(get_node_name)
  echo "changing name" | tee /tmp/step-bootstrap-$${WORKER_TYPE}
  echo $NAME > /etc/hostname
  echo "hostname: $(cat /etc/hostname)"
  set +euo pipefail
)}


#####
# SCRIPT
#####

# if there is an error
# call a lambda
# this lambda needs the error message and the instance id
# lambda should terminate the instance as userdata failed to complete
FUNCTION_LIST="install_worker_binaries tag_instance wait_for_files get_pki_files get_kubeconfig_files generate_node_certificate generate_k8s_config_files disable_swap get_pods_cidr configure_kube_router configure_containerd configure_kubelet configure_k8s_proxy copy_files_to_s3 start_k8s_worker_services update_cidrs_in_s3"
FUNCTION_LIST_ARRAY=($FUNCTION_LIST)

for function in "$${FUNCTION_LIST_ARRAY[@]}"
do
  $function
  ERROR="$?"
  if [[ "$ERROR" -ne 0 ]]
  then
    # call lambda error function
    curl_slack
    # shutdown now
    exit 1
  fi
done


#####
# CLEANUP
#####

rm /tmp/step-bootstrap-$${WORKER_TYPE}
