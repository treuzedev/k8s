Content-Type: multipart/mixed; boundary="//"
MIME-Version: 1.0

--//
Content-Type: text/cloud-config; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="cloud-config.txt"

#cloud-config
cloud_final_modules:
- [scripts-user, always]

--//
Content-Type: text/x-shellscript; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="userdata.txt"

#!/bin/bash

#####

#####
# PREPARATIONS
#####

echo "no step defined" > /tmp/step-userdata


#####
# FUNCTIONS
#####

function curl_slack {
  set -euo pipefail
  STEP=$(cat /tmp/step-userdata)
  curl \
    -s \
    -X POST \
    -H "Content-type: application/json" \
    -d "{
      \"text\": \"Error in function '$function' step '$STEP' with exit code '$ERROR'.\nInstance Id: '$ID'.\"
    }"\
    $SLACK_WEBHOOK_URL
  set +euo pipefail
}

function install_awscli {(
  set -euo pipefail
  echo "- installing awscli.."
  if [[ ! -f "/usr/bin/aws" ]];
  then
    curl -s "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    unzip awscliv2.zip
    ./aws/install
    mv /usr/local/bin/aws /usr/bin
    rm -rf aws awscliv2.zip
    aws --version
  else
    echo "awscli already installed."
    echo "testing.."
    aws --version
  fi
  echo "installing awscli successful!"
  set +euo pipefail
)}

function install_dependencies {(
  set -euo pipefail
  echo "- installing dependencies.."
  echo "updating with apt-get.."
  apt-get update -y
  apt-get upgrade -y
  echo "installing nginx, unzip, jq and wget.."
  apt-get install -y nginx unzip jq wget
  echo "testing (nginx will be tested later on)"
  jq --version
  wget --version
  unzip --hh
  echo "jq installed successfully!"
  if [[ ! -f "/usr/bin/cfssl" ]]
  then
    echo "installing cfssl.."
    curl -s "https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/1.4.1/linux/cfssl" -o "cfssl"
    chmod +x cfssl
    mv cfssl /usr/bin/
    echo "testing.."
    cfssl version
    echo "installing cfssl successful!"
  else
    echo "cfssl already installed!"
    echo "testing.."
    cfssl version
  fi
  if [[ ! -f "/usr/bin/cfssljson" ]]
  then
    echo "installing cfssljson.."
    curl -s "https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/1.4.1/linux/cfssljson" -o "cfssljson"
    chmod +x cfssljson
    mv cfssljson /usr/bin/
    echo "testing.."
    cfssljson --version
    echo "installing cfssljson successful!"
  else
    echo "cfssljson already installed!"
    echo "testing.."
    cfssljson --version
  fi
  echo "installing dependencies successful!"
  set +euo pipefail
)}

function run_user_data {(
  set -euo pipefail
  echo "downloading userdata from s3"
  aws s3 cp s3://$CONTROL_PLANE_USER_DATA_S3_PATH /tmp
  echo "userdata:"
  echo "---"
  cat /tmp/control-plane.sh
  echo "---"
  chmod 700 /tmp/control-plane.sh
  echo "running userdata"
  /tmp/control-plane.sh
  echo "cleaning up"
  rm /tmp/control-plane.sh
  set +euo pipefail
)}

function wait_for_files {(
  set -euo pipefail
  echo "- waiting for userdata files to be in s3 bucket.."
  FLAG="false"
  while [[ "$FLAG" == "false" ]]
  do
    OBJECTS=$(aws s3 ls s3://$BUCKET/userdata --recursive --summarize | grep 'Total Objects:' | awk '{print $NF}')
    if [[ "$OBJECTS" == $NUMBER_OF_USERDATA_FILES ]]
    then
      echo "files are ready!"
      FLAG="true"
    else
      echo "files not ready yet.."
      sleep 10
    fi
  done
  set +euo pipefail
)}


#####
# SCRIPT
#####

export BUCKET=${bucket}
export CONTROL_PLANE_USER_DATA_S3_PATH="${control_plane_user_data_s3_path}"
export NUMBER_OF_USERDATA_FILES="${number_of_userdata_files}"
export SLACK_WEBHOOK_URL="${slack_webhook_url}"
export ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)

# TODO:
# install awscli first
# if this fails, the node should be removed manually? or should it terminate simply?
# this obviously needs improvements
FUNCTION_LIST="install_dependencies install_awscli wait_for_files run_user_data"
FUNCTION_LIST_ARRAY=($FUNCTION_LIST)

for function in "$${FUNCTION_LIST_ARRAY[@]}"
do
  $function
  ERROR="$?"
  if [[ "$ERROR" -ne 0 ]]
  then
    # call lambda error function
    curl_slack
    # shutdown now
    exit 1
  fi
done


#####
# CLEANUP
#####

rm /tmp/step-userdata

#####

--//