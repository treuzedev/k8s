# create a bucket to store cluster files
resource "aws_s3_bucket" "cluster" {
  bucket = var.bucket_name
  acl    = "private"
  force_destroy = true
  tags = var.common_tags
}
