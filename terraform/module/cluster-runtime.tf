resource "aws_autoscaling_group" "cluster_runtime" {

  depends_on = [
    aws_instance.control_plane,
    null_resource.pki,
    null_resource.kubeconfig
  ]

  name = var.cluster_runtime.name
  max_size = var.cluster_runtime.max_size
  min_size = var.cluster_runtime.min_size
  desired_capacity = var.cluster_runtime.desired_capacity
  vpc_zone_identifier = [for subnet in var.cluster_runtime.subnets : aws_subnet.main[subnet].id]
  
  launch_template {
    version = "$Latest"
    id = aws_launch_template.cluster_runtime.id
  }
  
}

resource "aws_launch_template" "cluster_runtime" {

  name = var.cluster_runtime.name
  image_id = var.cluster_runtime.image_id
  instance_type = var.cluster_runtime.instance_type
  key_name = var.key_pair_name
  user_data = base64encode(templatefile("${path.module}/files/cluster-runtime-user-data.sh", {
    cluster_runtime_user_data_s3_path = "${var.bucket_name}/userdata/cluster-runtime.sh"
    slack_webhook_url = var.slack_webhook_url
    bucket = var.bucket_name
    number_of_userdata_files = var.number_of_userdata_files
  }))
  vpc_security_group_ids = concat([for name in setintersection(keys(aws_security_group.main), var.cluster_runtime.sgs) : aws_security_group.main[name].id], [aws_security_group.lb.id])
  tags = var.common_tags
  
  block_device_mappings {
    device_name = "/dev/sda1"
    ebs {
      volume_size = var.cluster_runtime.volume_size
      delete_on_termination = true
    }
  }
  
  iam_instance_profile {
    arn = aws_iam_instance_profile.cluster_runtime_instance_profile.arn
  }
  
}

resource "aws_s3_bucket_object" "cluster_runtime_user_data_s3" {
  depends_on = [aws_s3_bucket.cluster]
  bucket = var.bucket_name
  key    = "userdata/cluster-runtime.sh"
  content = templatefile("${path.module}/files/cluster-runtime-user-data-s3.sh", {
    slack_webhook_url = var.slack_webhook_url
    pki_files_dir = var.pki_files_dir
    kubeconfig_files_dir = var.kubeconfig_files_dir
    workers_files_dir = var.workers_files_dir
    number_of_pki_files = var.number_of_pki_files
    number_of_kubeconfig_files = var.number_of_kubeconfig_files
    c = var.certs.country
    l = var.certs.locality
    nat_gateway_id = aws_nat_gateway.main[var.ngw_subnet].id
    bucket = var.bucket_name
    lb_public_ip = aws_lb.main.dns_name
    cluster_name = var.cluster_name
    cluster_cidr = var.cluster_cidr
    worker_type = var.cluster_runtime.worker_type
  })
}

resource "aws_iam_instance_profile" "cluster_runtime_instance_profile" {
  name = "${var.cluster_name}-clusterruntime-instanceprofile"
  role = aws_iam_role.cluster_runtime_node_role.name
  tags = var.common_tags
}

resource "aws_iam_role" "cluster_runtime_node_role" {
  name = "${var.cluster_name}-clusterruntime-instanceprofile"
  tags = var.common_tags
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
  inline_policy {
    name = "cluster_runtime_node_role"
    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = [
            "ec2:CreateTags",
            "ec2:DescribeTags",
            "ec2:DescribeInstances",
            "ec2:DescribeNatGateways"
          ]
          Effect   = "Allow"
          Resource = "*"
        },
        {
          Action   = [
            "s3:ListBucket",
            "s3:GetObject",
            "s3:PutObject"
          ]
          Effect   = "Allow"
          Resource = [
            "arn:aws:s3:::${var.bucket_name}",
            "arn:aws:s3:::${var.bucket_name}/*"
          ]
        },
      ]
    })
  }
}
