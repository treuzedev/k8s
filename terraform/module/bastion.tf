# bastion instance to access private nodes
resource "aws_instance" "bastion" {
  subnet_id = aws_subnet.main[var.bastion.subnet].id
  vpc_security_group_ids = [for name in setintersection(keys(aws_security_group.main), var.bastion.sgs) : aws_security_group.main[name].id]
  ami = var.bastion.ami
  instance_type = var.bastion.instance_type
  key_name = var.key_pair_name
  associate_public_ip_address = true
  tags = merge(var.common_tags, var.bastion.extra_tags)
}

resource "aws_eip" "bastion" {
    vpc  = true
    tags = var.common_tags
}

resource "aws_eip_association" "bastion_eip_association" {
  instance_id   = aws_instance.bastion.id
  allocation_id = aws_eip.bastion.id
}
