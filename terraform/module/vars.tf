#####
# GENERAL VARS
#####

#####
variable "cluster_name" {
  description = "Cluster name. No defaults."
}

#####
variable "cluster_cidr" {
  description = "Cluster CIDR. No defaults."
}

#####
variable "slack_webhook_url" {
  description = "Slack endpoint for instances to send error messages. No defaults."
}


#####
# LB
#####

#####
variable "lb" {
  description = "A map defining the LB that fronts the cluster. No defaults"
}


#####
# VPC
#####

#####
variable "bastion" {
    description = "A map defining a bastion instance. No defaults."
}

#####
variable "key_pair_name" {
    description = "AWS Key Pair name. No defaults."
}

#####
variable "key_pair_content" {
    description = "AWS Key Pair content. No defaults."
}

#####
variable "vpc_cidr_block" {
    description = "VPC CIDR block. No defaults."
}

#####
variable "common_tags" {
    description = "VPC module common tags. No defaults."
}

#####
variable "enable_dns_support" {
    description = "Enable DNS support on a VPC. Defaults to true"
    default = true
}

#####
variable "enable_dns_hostnames" {
    description = "Enable DNS hostnames on a VPC. Defaults to true"
    default = true
}

#####
variable "subnets" {
    description = "A map, containing a another map for each key, where the key is the subnet name and the value a map configuring the subnet. No defaults."
}

#####
variable "rts" {
    description = "A map, containing maps that configure a route table. No defaults."
}

#####
variable "sgs" {
    description = "A map, containing maps configuring a security group. No defaults."
}

#####
variable "nats" {
    description = "A list, containing the names of the subnets where a NAT Gateway should exist. No defaults."
}

#####
variable "ngw_subnet" {
  description = "The name of the subnet where the K8s NGW lives. No defaults."
}

#####
variable "control_plane" {
    description = "A map defining the Control Plane nodes. No defaults."
}

#####
variable "cluster_runtime" {
    description = "A map defining the Cluster Runtime worker nodes. No defaults."
}

#####
variable "pki_role_name" {
    description = "The name of the Lambda execution role that provisions the necessary PKI infrastructure. No defaults."
}

#####
variable "kubeconfig_role_name" {
    description = "The name of the Lambda execution role that provisions the necessary kubeconfig files. No defaults."
}

#####
variable "bucket_name" {
  description = "The bucket name to store cluster files. No defaults."
}

#####
variable "pki_files_dir" {
  description = "The bucket dir name to store PKI infrastructure. No defaults."
}

#####
variable "workers_files_dir" {
  description = "The bucket dir name to store worker nodes files. No defaults."
}

#####
variable "number_of_pki_files" {
  description = "The expected number of PKI files in the bucket. No defaults."
}

#####
variable "number_of_kubeconfig_files" {
  description = "The expected number of kubeconfig files in the bucket. No defaults."
}

#####
variable "number_of_userdata_files" {
  description = "The expected number of userdata files in the bucket. No defaults."
}

#####
variable "kubeconfig_files_dir" {
  description = "The bucket dir to store kubeconfig files. No defaults."
}

#####
variable "pki_function_name" {
  description = "The name of the Lambda function that provisions the PKI infrastructure. No defaults."
}

#####
variable "kubeconfig_function_name" {
  description = "The name of the Lambda function that provisions kubeconfig files. No defaults."
}

#####
variable "pki_ecr_repo_name" {
  description = "The name of the ECR repo holding the PKI cfssl image. No defaults."
}

#####
variable "kubeconfig_ecr_repo_name" {
  description = "The name of the ECR repo holding the kubectl layer. No defaults."
}

#####
variable "pki_ecr_image_tag" {
  description = "The tag of the PKI cfssl image. No defaults."
}

#####
variable "kubeconfig_ecr_image_tag" {
  description = "The tag of the kubectl layer image. No defaults."
}

#####
variable "certs" {
  description = "A map configuring the certificates provisioning. No defaults."
}

#####
variable "run_pki_lambda" {
  description = "A string that allows the PKI resources provision by the corresponding lambda to be recreated each time terraform apply is ran. This only happens if the value is 'always'. Any other value only runs lambda the first time the underlying 'null_resource' is created. No defaults"
}


#####
variable "run_kubeconfig_lambda" {
  description = "A string that allows the kubeconfig files provision by the corresponding lambda to be recreated each time terraform apply is ran. This only happens if the value is 'always'. Any other value only runs lambda the first time the underlying 'null_resource' is created. No defaults"
}

#####
variable "control_plane_app_tag" {
  description = "App tag value passed to control plane nodes to allow them to discover other control plane nodes. No defaults."
}

#####
variable "control_plane_component_tag" {
  description = "Component tag value passed to control plane nodes to allow them to discover other control plane nodes. Also used to tag instances apart from the common tags. No defaults."
}

#####
variable "control_plane_environment_tag" {
  description = "Environment tag value passed to control plane nodes to allow them to discover other control plane nodes. No defaults."
}
