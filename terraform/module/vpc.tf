# create vpc
resource "aws_vpc" "main" {
    cidr_block = var.vpc_cidr_block
    enable_dns_support = var.enable_dns_support
    enable_dns_hostnames = var.enable_dns_hostnames
    tags = var.common_tags
}

# create vpc's igw
resource "aws_internet_gateway" "main" {
    vpc_id = aws_vpc.main.id
    tags = var.common_tags
}

# create nats
resource "aws_nat_gateway" "main" {
  for_each = toset(var.nats)
  depends_on = [aws_internet_gateway.main]
  allocation_id = aws_eip.nat[each.key].id
  subnet_id     = aws_subnet.main[each.key].id
  tags = var.common_tags
}

resource "aws_eip" "nat" {
    for_each = toset(var.nats)
    depends_on = [aws_internet_gateway.main]
    vpc  = true
    tags = var.common_tags
}

# create subnets
resource "aws_subnet" "main" {
    for_each = var.subnets
    vpc_id = aws_vpc.main.id
    availability_zone = each.value.az
    cidr_block = each.value.cidr
    tags = var.common_tags
}

# create rts
resource "aws_route_table" "main" {

    for_each = var.rts

    vpc_id = aws_vpc.main.id

    dynamic "route" {
        for_each = each.value.routes
        content {
            cidr_block = route.value.cidr
            gateway_id = route.value.type == "igw" ? aws_internet_gateway.main.id : ""
            nat_gateway_id = route.value.type == "nat" ? aws_nat_gateway.main[route.value.subnet].id : ""
        }
    }

    tags = var.common_tags

}

# associate subnets with rts
resource "aws_route_table_association" "main" {

    for_each = var.subnets

    subnet_id = aws_subnet.main[each.key].id
    route_table_id = aws_route_table.main[each.value.rt_name].id

}

# create sgs
resource "aws_security_group" "main" {

    for_each = var.sgs
    name = each.value.name
    description = each.value.description
    vpc_id = aws_vpc.main.id
    tags = var.common_tags

    dynamic "ingress" {
        for_each = try(each.value.ingress, {})
        content {
            from_port = ingress.value.from_port
            to_port = ingress.value.to_port
            cidr_blocks = try(ingress.value.cidr_blocks, []) 
            protocol = try(ingress.value.protocol, -1)
            description = try(ingress.value.description, "No description provided")
            self = try(ingress.value.self, true)
        }
    }

    dynamic "egress" {
        for_each = try(each.value.egress, {})
        content {
            from_port = egress.value.from_port
            to_port = egress.value.to_port
            cidr_blocks = try(egress.value.cidr_blocks, []) 
            protocol = try(egress.value.protocol, -1)
            description = try(egress.value.description, "No description provided")
            self = try(egress.value.self, true)
        }
    }

}
