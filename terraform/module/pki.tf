#####
resource "aws_iam_role" "pki" {
  
  name = var.pki_role_name
  tags = var.common_tags
  
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = "STS1"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
  
  inline_policy {
    name = "${var.pki_role_name}-${var.bucket_name}-s3"
    policy = jsonencode(
      {
        "Version": "2012-10-17",
        "Statement": [
          {
            "Sid": "S3P1",
            "Action": "s3:*",
            "Effect": "Allow",
            "Resource": "arn:aws:s3:::${var.bucket_name}"
          },
          {
            "Sid": "S3P2",
            "Action": "s3:*",
            "Effect": "Allow",
            "Resource": "arn:aws:s3:::${var.bucket_name}/*"
          }
        ]
      }
    )
  }
  
  inline_policy {
    name = "${var.pki_role_name}-${var.bucket_name}-logs"
    policy = jsonencode(
      {
        "Version": "2012-10-17",
        "Statement": [
          {
            "Effect": "Allow",
            "Action": [
              "logs:CreateLogGroup",
              "logs:CreateLogStream",
              "logs:PutLogEvents"
            ],
            "Resource": "*"
            "Sid": "LogsP1",
          }
        ]
      }
    )
  }
  
}

# create a lambda function that provisions the necessary pki infrastructure
data "aws_ecr_repository" "pki" {
  name = var.pki_ecr_repo_name
}

resource "aws_lambda_function" "pki" {

  depends_on = [aws_s3_bucket.cluster]
  function_name = var.pki_function_name
  role = aws_iam_role.pki.arn
  package_type = "Image"
  image_uri = "${data.aws_ecr_repository.pki.repository_url}:${var.pki_ecr_image_tag}"
  timeout = 120
  memory_size = 256
  tags = var.common_tags
}

# provision ca and the admin client certificates
resource "null_resource" "pki" {

        # "lb_public_ip": "${local.lb_ip}",
        # "node_0_ip": "${local.private_ip_1}",
        # "node_1_ip": "${local.private_ip_2}",
        # "node_2_ip": "${local.private_ip_3}"
        
  triggers = {
    always_run = var.run_pki_lambda == "always" ? "${timestamp()}" : ""
  }

  depends_on = [
    aws_lambda_function.pki,
    aws_iam_role.pki,
  ]

  provisioner "local-exec" {
  
    command = <<EOT
    sleep 30 && \
    aws lambda invoke \
      --function-name ${var.pki_function_name} \
      --invocation-type Event \
      --cli-binary-format raw-in-base64-out \
      --payload '{
        "cert": {
          "O": "${var.certs.org}",
          "C": "${var.certs.country}",
          "L": "${var.certs.locality}"
        },
        "bucket": "${var.bucket_name}",
        "pki_files_dir": "${var.pki_files_dir}",
        "lb_public_ip": "${aws_lb.main.dns_name}",
        "node_0_ip": "${aws_instance.control_plane[0].private_ip}",
        "node_1_ip": "${aws_instance.control_plane[1].private_ip}",
        "node_2_ip": "${aws_instance.control_plane[2].private_ip}"
      }' \
      outfile
    EOT
    
    interpreter = ["/bin/bash", "-c"]
    
  }
  
}