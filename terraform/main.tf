#####
# K8S CLUSTER
#####

module "k8s_cluster" {

  source = "./module"
  
  cluster_name = var.cluster_name
  cluster_cidr = var.cluster_cidr
  bucket_name = var.bucket_name
  slack_webhook_url = var.slack_webhook_url
  
  bastion = var.bastion
  
  key_pair_name = var.key_pair_name
  key_pair_content = var.key_pair_content

  vpc_cidr_block = var.vpc_cidr_block
  subnets = var.subnets
  rts     = var.rts
  sgs     = var.sgs
  nats = var.nats
  ngw_subnet = var.ngw_subnet
  
  lb = var.lb
  
  control_plane = var.control_plane
  control_plane_app_tag = var.control_plane_app_tag
  control_plane_component_tag = var.control_plane_component_tag
  control_plane_environment_tag = var.control_plane_environment_tag
  number_of_userdata_files = var.number_of_userdata_files
  
  workers_files_dir = var.workers_files_dir
  cluster_runtime = var.cluster_runtime
  
  run_pki_lambda = var.run_pki_lambda
  run_kubeconfig_lambda = var.run_kubeconfig_lambda
  
  pki_files_dir = var.pki_files_dir
  number_of_pki_files = var.number_of_pki_files
  pki_function_name = var.pki_function_name
  pki_role_name = var.pki_role_name
  pki_ecr_repo_name = var.pki_ecr_repo_name
  pki_ecr_image_tag = var.pki_ecr_image_tag
  certs = var.certs
  
  kubeconfig_files_dir = var.kubeconfig_files_dir
  number_of_kubeconfig_files = var.number_of_kubeconfig_files
  kubeconfig_function_name = var.kubeconfig_function_name
  kubeconfig_role_name = var.kubeconfig_role_name
  kubeconfig_ecr_repo_name = var.kubeconfig_ecr_repo_name
  kubeconfig_ecr_image_tag = var.kubeconfig_ecr_image_tag

  common_tags = var.k8s_tags

}